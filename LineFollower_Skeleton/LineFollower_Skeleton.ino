
/* Line Following Robot 2016 */

/*
 * Precompiler defines are used to set configuration values. Whenever one of these values appears in code, it is replaced with it's value.
 * This is useful for values representing hardware configurations and calibration/tuning parameters. If any of these parameters needs to change
 * then they only need be changed in one location, instead of everywhere that they are used. 
 * 
 * In fact, you can even write macros and more advanced things in precompiler defines. But we won't go that deep. (it is actually what I have done when you use the checkSensors() function
 */

// Hardware pin defines. Specify the arduino pin numbers that the motors and sensors connect to.
#define L_MOTORS 10        // Left motor PWM Pin
#define R_MOTORS 11        // Right motor PWM pin
#define IR_L1 6
#define IR_L2 7
#define IR_L3 8
#define IR_L4 9
#define IR_R1 5
#define IR_R2 4
#define IR_R3 3
#define IR_R4 2


// Define some different default speeds.
// These are to you to play with. You can create as many different speeds defaults that you want.
// You don't need to use these defines to set the speed, you can just write the number directly in code. But often you will want to use the same
// speed in multiple places (eg you want both motors to go the same speed).
// So if you define the speed here and then use a name to represent it, changing the one number here will change the value everywhere.
// But if you have the speeds just as number in your code, then you may accidently forget to update one. Then one motor is going a different speed to the other 
// and it can be really hard to debug why when all you are looking at is a bunch of numbers.
// 
#define FULL_SPEED 0x60   // Full speed PWM
#define HALF_SPEED 0x40   // Half speed PWM
#define QUARTER_SPEED 0x20   // Quarter speed PWM

#define LOW_SPEED   0x10 // Lowest Speed


/*
 * Global variables.
 */
uint8_t IR_Left1;     // Variable to store the digital state of the left inner line sensor
uint8_t IR_Left2;     // Variable to store the digital state of the left outer line sensor
uint8_t IR_Left3;     // \/ \/ etc, etc \/ \/
uint8_t IR_Left4; 

uint8_t IR_Right1;     
uint8_t IR_Right2;     
uint8_t IR_Right3; 
uint8_t IR_Right4; 

uint8_t lastSensor;

char buff[200];           // Allocate a buffer to generate and output debug strings

/* ---------------------------------------------------------------
 * Don't worry about how this works. I wrote it just to make reading the sensors easier.
 * The sensors can be quite complex to read if you just use Arduino code, so this is some lower level stuff to save some time.
 * You have enough to learn already I think :)
 */
uint8_t sensors;
#define checkSensor(x) (sensors >> (x-2))&0x01
#define RC_DECAY 1500
uint8_t readIR(){
  DDRD |= 0xFC;
  DDRB |= 0x03;
  PORTD |= 0xFC;
  PORTB |= 0x03;
  _delay_us(10);
  DDRD &= 0x03;
  DDRB &= 0xFC;
  _delay_us(RC_DECAY);
  sensors = PIND >> 2 | ((PINB & 0x03) << 6);
  return sensors;
  }
// ----------------------------------------------------------------



/*
 * Setup function. This function is run first, as soon as the controller turns on.
 * You should set up the controller in this function.
 * Things to do:
 *    Start the serial connection so that we can send debug strings. Hint: Serial.begin();
 *                         (p.s. serial comms is already done for you :) )
 *    Set the hardware pins to be either inputs or outputs.  Hint: pinMode();
 */
void setup() {
  // Start serial communications at 9600 baud.
  Serial.begin(9600);

  
  // Set Pin IO  
  /*
   *  There are two motors, each one needs to be controlled with a PWM signal.
   *  A pwm signal sets a voltage by turning on and off the pin really fast.
   *  If it is on for half of the time and off for the other half, then on average the output voltage will be half of the maximum.
   *  Since the speed of the motor depends on the voltage that you give it, this means it will be half of maximum speed.
   *  
   *  Similarly, if you only turn the pin on for a quarter of the time, then the average voltage will only be a quarter of maximum. 
   *  Three quarters of the time will be three quarters of the maximum -- so on.
   *  
   *  The ammount of time that a signal is on is called the DUTY CYCLE. It is measured in a percentage, where 100% is full and 0% os off.
   *  
   *  To be able to output the PWM signal on a pin, you first need to set the pin to be in OUTPUT mode.
   *  In Arduino, you can use the 'pinMode' command to achieve this.
   *  It looks like this 'pinMode( pin, direction );'
   *  The 'pinMode' command takes two input parameters, the first is the pin that you want to set, the second is the direction that you wanty to set it to (INPUT or OUTPUT).
   *  Since we used a precompiler define to remember which pin controls each motor, we can use L_MOTORS or R_MOTORS as the pin. You could just as easily put a number instead,
   *  but if you use the pin in multiple locations - as we will - and you want to change the pin later, then it is easier to just change the precompiler define instead of 
   *  go through the code and try and change everywhere that it is used - and risk missing one!
   *  
   *  TODO:
   *  You will need to use the pinMode command to set the L_MOTORS and R_MOTORS pins to OUTPUT
   *  
   */

  
  // Set the PWM pins to output mode

/*
 * We also use digital pins to read the sensors. However I have written the readIR() function to do this. This function operates by 
 * directly detting the hardware registers of the chip and it handles all of the pin I/O, so you don't need to set the pins to be inputs here.
 * But just know that if we weren't using that readIR() function, you would also need to set up the pins to read the sensors here.
 */
}




/*
 * After the setup function has been run, the loop function will begin to be called over and over again.
 * This is the place where all of the action happens. Here you will want to:
 *    -Check the value of the line sensors.
 *      Hint: The readIR() function will automatically read all 8 sensors and save their value into a global variable named 'senors'.
 *            You don't really need to use that global variable 'sensors' because the 'checkSensor()' function already knows to look at it.
 *      
 *    -Use some logic to determine which way you want the motors to turn, based on what the line sensors tell you.
 *      Hint: The 'If' statement can be used to do one thing if a statement is true, and another thing if it is false.
 *            The 'checkSensor(sensor name)' can be used to check if a particular sensor sees a black line. It will return true if it sees a black line, but false if it doesn't
 *      
 *    -Set the left and right motor speed
 *      Hint: The analogWrite(_pinnumber_, _value_) function can be used to write a value between 0 and 255 to a PWM pin. 
 *            This number will set the voltage that the pin outputs (and therefore speed of the motor. more volts = more speed!)
 *            So writing 0 will stop the motor. writing 255 will make it go at full speed, writing 128 will set it half speed and so on.
 *            
 *    -Optional but recommended: Print some debugging information to the console. (We will help you with this)
 *    
 *    
 *  There are many, MANY different ways that the logic can be written to follow a line.
 *  The simplest way is to look at one sensor on the left and one on the right
 *    If the line is seen on the left then turn the left motor on and the right motor off.
 *    If the line is seen on the right then turn the right motor on and the left motor off.
 *    This will make the robot jump left and right, but will slowly move forward along the line.
 *    
 *    You have four sensors on the left and four on the right. So you can set speeds to turn a little bit if you only see on e of the inner sensors,
 *    but turn really hard if you see one of the outer sensors. 
 *        
 */
void loop() {


  /*
   * The first thing to do in out loop is to check what the sensors are reading.
   * The sensors use an RC decay rate to represent the ammount of light they see. You don't need to worry about this.
   * Using some hardware level magic, I have written a funtion that will put all the of the sensors values in the 'sensors' variable.
   * You can then check an individual sensor by typing checkSensor(sensor name) and it will give you a true if there is a line or a false otherwise.
   * 
   * TODO: 
   * 
   *  Use readIR(); to read sensor data.
   * 
   */
   
  // Read all 8 sensors at once.
  readIR();



  /*
   * Now that we know where the line is, we can decide what speed to set out motors at.
   * 
   * Some example logic is as follows:
   * This assumes that the line is within the sensor range, if it is not then the robot will just run away. So you will need to make improvements.
   *  If the line is detected at one of the inner sensors, then we want to turn slowly to center it.
   *    --> Left inner sensor detects line, turn slowly left.
   *    --> Right inner sensor detects line, turn slowly right.
   *  But if a line is detected at one of the outer sensors then we want to turn hard to center it.
   *    --> Left outer sensor detects line, turn hard left.
   *    --> Right outer sensor detects line, turn hard right.
   *    
   *  
   *  You can set the PWM value on one of the motors pins (ie, set the motor speed) using the analogWrite function,
   *  analogWrite(pin, number between 0 and 255)
   *  
   *  
   */




  /*
   * Some debugging info. This will print 8 numbers, either 0 or 1, representing if a line is seen on each of the 8 sensors.
   * It will also print a number between 0 and 255 that represents the integer representation of all of those 8 1's and 0's. 
   */
  buff[0] = '\0'; // clear the string before we start writing to it
  for (uint8_t i = 0; i < 8; i++)
  {
    sprintf(buff, "%s%d", buff, (sensors >> i) & 0x01 ? 1 : 0);
  }
  Serial.write(buff);
  sprintf(buff, " - %d\n", sensors);
  Serial.write(buff);
  
  // This delay is not really necessary for line following, but it is usefull to slow down the debug strings.
  // In fact, it actually hurts line following because the delay makes things jumpy.
  // I suggest commenting this line out by putting a // in front if it before actually get the robot to properly follow a line.
  _delay_ms(10);

}





